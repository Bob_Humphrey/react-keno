import { configureStore } from "@reduxjs/toolkit";
import cardReducer from "./slices/gameSlice";

export default configureStore({
  reducer: {
    card: cardReducer
  }
});
